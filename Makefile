cuda-go-example: libmaxmul.so
	go build .

libmaxmul.so: gpu_code.cu
	nvcc --ptxas-options=-v --compiler-options '-fPIC' -o $@ --shared $?

run:
	LD_LIBRARY_PATH=${PWD} ./cuda-go-example
